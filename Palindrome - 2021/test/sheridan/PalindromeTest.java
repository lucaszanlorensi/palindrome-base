package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean isPalindrome = Palindrome.isPalindrome("anna");
		assertTrue("Unable to validate Palindrome.", isPalindrome);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Derek");
		assertFalse("Unable to validate Palindrome.", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Race car");
		assertTrue("Unable to validate Palindrome.", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Race a car");
		assertFalse("Unable to validate Palindrome.", isPalindrome);
	}	
	
}
